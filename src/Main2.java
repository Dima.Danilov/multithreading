public class Main2 {
    private static Long counter = 0L;

    public static void main(String[] args) {
        new CounterThread().start();
        new ReaderThread().start();
    }

    private static class CounterThread extends Thread {
        @Override
        public void run() {
            while (true) {
                counter++;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while (true) {
                System.out.println(counter);
            }
        }
    }
}
