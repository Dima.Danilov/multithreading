public class Consumer extends Thread {
    private String name;

    public Consumer(String name) {
        super(name);
    }

    @Override
    public void run() {
        while (true) {
            Task task = Main.getTask();
            if (task != null) {
                System.out.println(Thread.currentThread().getName() + " get started task " + task);
                try {
                    Thread.sleep(task.getDuration());
                } catch (InterruptedException ex) {
                    System.out.println(this + " is interrupted");
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Consumer{" +
                "name='" + name + '\'' +
                '}';
    }
}
