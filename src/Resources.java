import java.util.ArrayDeque;
import java.util.Deque;

public class Resources {
    private static Resources instance;
    private volatile Deque<Task> taskArrayDeque;

    private Resources() {
        this.taskArrayDeque = new ArrayDeque<>();
    }

    public static Resources getInstance() {
        if (instance == null) {
            synchronized (Resources.class) {
                if (instance == null) {
                    instance = new Resources();
                }
            }
        }
        return instance;
    }

    public Task newTask() {
        Task task = new Task();
        taskArrayDeque.addLast(task);
        System.out.println(task + " added"
                + ", queue length: " + taskArrayDeque.size());
        return task;
    }

    public Task getTask() {
//        synchronized (this) {
        return taskArrayDeque.pollLast();
//        }
    }
}
