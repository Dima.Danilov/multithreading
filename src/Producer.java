import java.util.Random;

public class Producer extends Thread {

    public Producer() {
        setName("iAmProducer");
    }

    @Override
    public void run() {
        while (true) {
            if (new Random().nextDouble() < 0.25) {
                Main.newTask();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}