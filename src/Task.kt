import java.util.Random

class Task {
    val name: String
    val duration: Long

    init {
        this.name = "Task ${counter++}"
        this.duration = (Random().nextInt(3000) + 2000).toLong()
//        counter++
    }

    override fun toString(): String {
        return "Task{" +
                "name='" + name + '\''.toString() +
                ", duration=" + duration +
                '}'.toString()
    }

    companion object {
        private var counter = 0
    }
}
