import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main3 {
    private static List<Double> doubleList = new ArrayList<>();

    public static Object o = new Object();

    public static void main(String[] args) {
        new AddThread().start();
        new ReadThread().start();
    }

    private static class AddThread extends Thread {
        @Override
        public  void run() {
            while (true) {
                synchronized (Main.class) {
                    doubleList.add(new Random().nextDouble() * 1000);
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class ReadThread extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                double averageValue = 0;
                synchronized (Main.class) {
                    for (Double item : doubleList) {
                        averageValue += item;
                        System.out.println("Average value: " + averageValue / doubleList.size());
                    }
                }
            }
        }
    }
}
