import java.util.ArrayDeque;
import java.util.Deque;

public class Main {
    public static volatile Deque<Task> taskDeque = new ArrayDeque<>();

    public static Task newTask() {
        Task task = new Task();
        taskDeque.addLast(task);
        System.out.println(task + " added"
                + ", queue length: " + taskDeque.size());
        return task;
    }

    public static Task getTask() {
        Task task = taskDeque.pollFirst();
//        synchronized (this) {
        try{
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return task;
//        }
    }

    public static void main(String[] args) {
        Producer producer = new Producer();
        Consumer consumer1 = new Consumer("Consumer #1");
        Consumer consumer2 = new Consumer("Consumer #2");
        producer.start();
        consumer1.start();
        consumer2.start();
        System.out.println("Finished " + Thread.currentThread().getName());
    }
}
